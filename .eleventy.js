module.exports = function (eleventyConfig) {

  // eleventyConfig.addFilter("searchSingle", searchFilterSingle);
  eleventyConfig.addCollection("preprints", (collection) => {
    return [
      ...collection.getFilteredByGlob("./src/content/preprints/**.html"),
    ].sort((a, b) => new Date(b?.date) - new Date(a?.date));
  });

  eleventyConfig.addCollection("customhtml", (collection) => {
    return [...collection.getFilteredByGlob("./src/content/customhtml/**.html")];
  });

  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/outputs": "/outputs" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });

  eleventyConfig.addFilter("getDOI", (rawString) => rawString.slice(13, -1) ?? rawString
  );

  // plugin TOC

  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};
