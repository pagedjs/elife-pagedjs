// const { exec } = require('child_process');
const fs = require("fs");
const { got } = require("got-cjs");
const jsdom = require("jsdom");
const { slugify, getFileName, info, error, warn, success } = require("./utils");

const { JSDOM } = jsdom;

// const host = "https://staging--epp.elifesciences.org/reviewed-preprints";
const host = "";
const urls = [
  "https://elifesciences.org/reviewed-preprints/88799/pdf",
  "https://elifesciences.org/reviewed-preprints/88744/pdf",
  "https://elifesciences.org/reviewed-preprints/88955/pdf",
  "https://elifesciences.org/reviewed-preprints/89489/pdf",
  "https://elifesciences.org/reviewed-preprints/87736/pdf",
  "https://elifesciences.org/reviewed-preprints/87881/pdf",
  "https://elifesciences.org/reviewed-preprints/88652/pdf",
  "https://elifesciences.org/reviewed-preprints/89267/pdf",
];
// const generatedDOIs = []
scrap(urls);

async function scrap(urls) {
  urls.forEach(async (url, index) => {
    got(url)
      .then(async (response) => {
        try {
          const id = await createSinglePage(response.body, index);
          success(`${id} - initial process completed`);
        } catch (err) {
          error(`${url} an error occurred`, err);
        }
      })
      .catch((err) => {
        error(`${url} error in url access`, err);
      });
  });
}

async function createSinglePage(content, index) {
  let doc = new JSDOM(content);
  let document = doc.window.document;

  const titleElem = document.querySelector("h1");
  const title = titleElem.innerHTML;
  // for all links check if there is a doi in there.
  // stop after the first doi

  const doi = document
    .querySelector(".descriptors a[href*=doi]")
    .innerHTML.split("-->")[1];

  // generatedDOIs.push(doi);
  document.querySelector(
    ".descriptors a[href*=doi]"
  ).innerHTML = `https://doi.org/${doi}`;

  // console.log(document.querySelector('.descriptors a[href*=doi]').innerHTML)

  if (index === 0) {
    const styleSrc = document
      .querySelector('link[rel="stylesheet"]')
      .getAttribute("href");
    got(`${host}${styleSrc}`)
      .then((response) => {
        fs.writeFileSync(`./static/css/default.css`, response.body);
      })
      .catch((err) => error(`error in ${id}`, err));
  }

  document.querySelectorAll("script").forEach((script) => {
    script.remove();
  });
  document.querySelectorAll("head").forEach((head) => {
    head.remove();
  });
  document.querySelectorAll("style").forEach((style) => {
    style.remove();
  });
  document.querySelectorAll("link").forEach((link) => {
    link.remove();
  });

  // addReviews(peerReviews)

  // if (theReviews) {
  //   document
  //     .querySelector('#author-list')
  //     .insertAdjacentHTML('afterend', theReviews)
  // }

  // get the first word of the title to get an ID
  const id = getFileName(slugify(doi));

  updatedImgURLs(document.documentElement, id);

  fs.writeFileSync(
    `./src/content/preprints/${id}.html`,
    `---\n
date: "${new Date().toISOString()}"
title: "${title}" \n
doi: "${doi}" \n
pdf: false\n
id: ${id}\n
peerReview: ""
---\n\n
${document.documentElement.innerHTML}`
  );

  if (!fs.existsSync(`./static/css/${id}.css`))
    fs.writeFileSync(`./static/css/${id}.css`, "");

  return id;
  // try {
  //   const resp = await generatePDF(doi)
  //   return resp;
  // } catch (error) {
  //   throw error;
  // }
  // change img urls
  // recreate the article from the part
  // remove scripts
}

function updatedImgURLs(content, id) {
  // test if the folder exists for that id exists
  const dir = `./static/images/${id}`;

  try {
    if (fs.existsSync(dir)) {
      console.log(`dir ${dir} exist`);
    } else {
      fs.mkdir(`static/images/${id}`, (err) => {
        if (err) {
          error(`error in ${id} - folder creation`, err);
          return;
        }
        info(`${id} folder created ok - note: images might still be streaming`);
      });
    }

    // use axios to get the images and render them.
    const callback = (id, img, index, err) =>
      error(`error in ${id} - ${index} image, src: ${img?.src}`, err);

    const { fetchAll, imgIndex } = refetchImages(id);
    if (!fetchAll && !imgIndex.length) return;
    content.querySelectorAll("img").forEach((img, index) => {
      try {
        if (imgIndex.length && !imgIndex.includes(index)) return;
        // remove styles on image
        img.style = "";

        if (!img.src) {
          warn(` ${id} - skipping image ${index} - no src`);
          return;
        }

        if (img.src?.[0] === "/") {
          warn(
            ` ${id} - image ${index} - added host - since it starts with /, src: ${img.src}`
          );
          img.src = `https://prod--epp.elifesciences.org/reviewed-preprints${img.src}`;
        }
        // download all images

        // check if it’s a base64 svg. then recreate a svg file with a fake name
        if (img.src.includes("data:image/svg+xml")) {
          let svgData = img.src.split("svg+xml,")[1];
          console.log(id, `in svg xml`, index);
          return console.log("data64 → " + svgData);
        } else if (img.src.includes("data:image/gif")) {
          let content = decodeBase64Image(img.src, id);
          console.log(id, `in b64`, index);
          fs.createWriteStream(
            `./static/images/${id}/gif-${index}.gif`,
            content,
            function (err) {
              if (err) {
                error(`err in ${id} - data:gif`, err);
              }
            }
          );
          return;
        } else if (img.src.includes(".gif")) {
          // create a src image
          let src = img.src.split("%2F")[2]?.split(".gif")[0];
          let imgURL = `${host}${img.src}`;
          console.log(id, `in gif`, index, imgURL);

          got
            .stream(imgURL)
            .pipe(
              fs.createWriteStream(`./static/images/${id}/img-${index}.gif`)
            );
          img.insertAdjacentHTML(
            "beforebegin",
            `<img src="{{ '../' | url }}images/${id}/img-${index}.gif" id="${
              img.id ? img.id : ""
            }">`
          );
          img.remove();
        } else if (img.src.includes(".svg")) {
          let src = img.src.split("%2F")[2]?.split(".tif")[0];

          let imgURL = `${host}${img.src}`;
          console.log(id, `in svg`, index, imgURL);

          got
            .stream(imgURL)
            .pipe(
              fs.createWriteStream(`./static/images/${id}/img-${index}.svg`)
            );
          img.insertAdjacentHTML(
            "beforebegin",
            `<img src="{{ '../' | url }}images/${id}/img-${index}.svg" id="${
              img.id ? img.id : ""
            }">`
          );
          img.remove();
        } else {
          // create a src image
          let src = img.src.split("%2F")[2]?.split(".tif")[0];

          let imgURL = `${host}${img.src}`;
          console.log(id, `in else`, index, imgURL);
          got
            .stream(imgURL)
            .on("error", function (err) {
              callback(id, img, index, err);
            })
            .pipe(
              fs.createWriteStream(`./static/images/${id}/img-${index}.jpg`)
            )
            .on("error", function (err) {
              callback(id, img, index, err);
            });
          img.insertAdjacentHTML(
            "beforebegin",
            `<img src="{{ '../' | url }}images/${id}/img-${index}.jpg" id="${
              img.id ? img.id : ""
            }">`
          );

          img.remove();
        }
      } catch (err) {
        error(`error in ${id} - ${index} image, src: ${img?.src}`, err);
      }
    });
    info("started image streaming...");
  } catch (err) {
    error(`err in ${id}`, err);
    return;
  }
}

/**
 * Decode base64 image
 * https://gist.github.com/barbietunnie/5fa07012925ee0fe53a0
 *.e.g. data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAPAQMAAABeJUoFAAAABlBMVEX///8AAABVwtN+AAAAW0lEQVQImY2OMQrAMAwDjemgJ3jI0CFDntDBGKN3hby9bWi2UqrtkJAk8k/m5g4vGBCprKRxtzQR3mrMlm2CKpjIK0ZnKYiOjuUooS9ALpjV2AjiGY3Dw+Pj2gmnNxItbJdtpAAAAABJRU5ErkJggg==
 */

function decodeBase64Image(dataString, id) {
  try {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};

    if (matches.length !== 3) {
      return new Error("Invalid input string");
    }

    response.type = matches[1];
    response.data = Buffer.from(matches[2], "base64");

    return response;
  } catch (err) {
    error(`err in ${id}`, err);
  }
}

function refetchImages(id) {
  console.log("id", id);
  /* NOTE:
   * solution not complete
   * Stage/commit previous HTML before re-running scrapper
   * Remove new HTML after scrap
   * */
  const images = {
    // 88189.1: [3, 15, 16, 17, 19, 20, 21, 22, 23, 24, 28, 29, 30, 32],
    // 86721.1: [2, 3, 4, 5, 8, 9],
    // 87120.1: [2, 3, 4, 6, 7, 11],
    // 87518.1: [4, 7],
    // 87536.1: [1, 4, 5, 6, 13],
    // 88183.1: [2, 3, 6, 7, 8, 13],
    // 88483.1: [3, 5],
    // 88658.1: [1, 4, 5, 7, 8, 10, 11, 12, 14, 15, 17],
  };
  if (!Object.keys(images).length) return { fetchAll: true, imgIndex: [] };
  if (images[id] && images[id]?.length)
    return { fetchAll: false, imgIndex: images[id] };
  return { fetchAll: false, imgIndex: [] };
}
