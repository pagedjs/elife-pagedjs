# html update

If we can have the html we want,  

article flags:
- separate the field (link-msa)
- separate the review tags (tour de force, landmark, etc.), because they need to
  be removed

authors: 
- needs to be a simple list
- needs to give the link between the author and the affiliation
- needs to have the corresponding authors marked 

affiliations:
- address are empty.

metadata:
- date is missing
- the copyright information is a link to the copyright, and not the copyright itself

abstract: 
- move it right after the title (or shouldn’t it appear right after the title?)

table of content: 
- removed


The reviewed preprint bit is missing.

done manually
removed the bit about elife review
images are converted from tif to jpg and reimplemented.


done with script 
add the bit about the preprint on the cover before the abstract
add the date before the doi on the cover

running head: 
- how do we chose authors to appear in the runnig head? How do they appear in the html
