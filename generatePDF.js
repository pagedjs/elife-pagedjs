const { exec } = require("child_process");
const { slugify, getFileName, error } = require("./utils");

const allDOI = [
  "10.7554/eLife.87736.1",
  "10.7554/eLife.87881.1",
  "10.7554/eLife.88652.1",
  "10.7554/eLife.88744.1",
  "10.7554/eLife.88799.1",
  "10.7554/eLife.88955.2",
  "10.7554/eLife.89267.1",
  "10.7554/eLife.89489.1",

  //----
  // todo
  // "10.7554/eLife.88403.1",
  // "10.7554/eLife.88984.1",
  // "10.7554/eLife.86324.2",
  //-------
];

const generatePDF = async (doi) => {
  if (!doi || typeof doi !== "string")
    error(
      `invalid DOI ${
        doi === null || doi === undefined ? doi : JSON.stringify(doi, null, 2)
      }`
    );

  try {
    const slugifiedDOI = slugify(doi);
    const outFileName = getFileName(slugifiedDOI);
    const baseScript = `pagedjs-cli ./public/${slugifiedDOI}/index.html --additional-script ./public/js/csstree.js --additional-script ./public/js/scripts.js -s`;
    const generatePDF = `${baseScript} -o  ./static/outputs/${outFileName}.pdf`;
    const debugPDF = `${baseScript} --debug`;

    exec(`${generatePDF}`, (error) => {
      if (error) {
        console.error(`--------- error in ${doi}---------`, error);
      }
      console.log(`------------${doi}.pdf created successfully----------`);
    });
  } catch (err) {
    error(`An error occurred - ${doi}`, err);
  }
};

allDOI.forEach(generatePDF);
